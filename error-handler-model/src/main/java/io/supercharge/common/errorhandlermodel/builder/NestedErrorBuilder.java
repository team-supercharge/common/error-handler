package io.supercharge.common.errorhandlermodel.builder;

import static java.util.Objects.nonNull;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import io.supercharge.common.errorhandlermodel.model.NestedError;

public final class NestedErrorBuilder {
    private String fieldName;
    private String message;
    private String code;
    private final Map<String, String> params;

    private NestedErrorBuilder() {
        params = new ConcurrentHashMap<>();
    }

    public static NestedErrorBuilder aNestedError() {
        return new NestedErrorBuilder();
    }

    //CHECKSTYLE:OFF
    public NestedErrorBuilder withFieldName(String fieldName) {
        this.fieldName = fieldName;
        return this;
    }

    public NestedErrorBuilder withMessage(String message) {
        this.message = message;
        return this;
    }

    public NestedErrorBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    public NestedErrorBuilder withParam(String name, String value) {
        params.put(name, value);
        return this;
    }

    public NestedErrorBuilder withParams(Map<String, String> additionalParams) {
        if (nonNull(additionalParams)) {
            params.putAll(additionalParams);
        }
        return this;
    }
    //CHECKSTYLE:ON

    public NestedError build() {
        NestedError nestedError = new NestedError();
        nestedError.setFieldName(fieldName);
        nestedError.setMessage(message);
        nestedError.setCode(code);
        nestedError.setParams(params);
        return nestedError;
    }
}
