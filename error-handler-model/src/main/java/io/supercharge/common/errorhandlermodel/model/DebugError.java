package io.supercharge.common.errorhandlermodel.model;

import java.util.Objects;

public class DebugError {
    private Long timestamp;
    private String stacktrace;
    private String url;
    private String requestBody;
    private String requestHeaders;
    private Integer upstreamStatusCode;

    public DebugError() {
    }

    public DebugError(
        Long timestamp,
        String stacktrace,
        String url,
        String requestBody,
        String requestHeaders,
        Integer upstreamStatusCode
    ) {
        this.timestamp = timestamp;
        this.stacktrace = stacktrace;
        this.url = url;
        this.requestBody = requestBody;
        this.requestHeaders = requestHeaders;
        this.upstreamStatusCode = upstreamStatusCode;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getStacktrace() {
        return stacktrace;
    }

    public void setStacktrace(String stacktrace) {
        this.stacktrace = stacktrace;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getRequestHeaders() {
        return requestHeaders;
    }

    public void setRequestHeaders(String requestHeaders) {
        this.requestHeaders = requestHeaders;
    }

    public Integer getUpstreamStatusCode() {
        return upstreamStatusCode;
    }

    public void setUpstreamStatusCode(Integer upstreamStatusCode) {
        this.upstreamStatusCode = upstreamStatusCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DebugError that = (DebugError) o;
        return Objects.equals(timestamp, that.timestamp)
            && Objects.equals(stacktrace, that.stacktrace)
            && Objects.equals(url, that.url)
            && Objects.equals(requestBody, that.requestBody)
            && Objects.equals(requestHeaders, that.requestHeaders)
            && Objects.equals(upstreamStatusCode, that.upstreamStatusCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp, stacktrace, url, requestBody, requestHeaders, upstreamStatusCode);
    }

}
