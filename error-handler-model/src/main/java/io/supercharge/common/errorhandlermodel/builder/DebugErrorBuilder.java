package io.supercharge.common.errorhandlermodel.builder;

import io.supercharge.common.errorhandlermodel.model.DebugError;

public final class DebugErrorBuilder {
    private Long timestamp;
    private String stacktrace;
    private String url;
    private String requestBody;
    private String requestHeaders;
    private Integer upstreamStatusCode;

    private DebugErrorBuilder() {
    }

    public static DebugErrorBuilder aDebugError() {
        return new DebugErrorBuilder();
    }

    //CHECKSTYLE:OFF
    public DebugErrorBuilder withTimestamp(Long timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public DebugErrorBuilder withStacktrace(String stacktrace) {
        this.stacktrace = stacktrace;
        return this;
    }

    public DebugErrorBuilder withUrl(String url) {
        this.url = url;
        return this;
    }

    public DebugErrorBuilder withRequestBody(String requestBody) {
        this.requestBody = requestBody;
        return this;
    }

    public DebugErrorBuilder withRequestHeaders(String requestHeaders) {
        this.requestHeaders = requestHeaders;
        return this;
    }

    public DebugErrorBuilder withUpstreamStatusCode(Integer upstreamStatusCode) {
        this.upstreamStatusCode = upstreamStatusCode;
        return this;
    }
    //CHECKSTYLE:ON

    public DebugError build() {
        DebugError debugError = new DebugError();
        debugError.setTimestamp(timestamp);
        debugError.setStacktrace(stacktrace);
        debugError.setUrl(url);
        debugError.setRequestBody(requestBody);
        debugError.setRequestHeaders(requestHeaders);
        debugError.setUpstreamStatusCode(upstreamStatusCode);
        return debugError;
    }
}
