package io.supercharge.common.errorhandlermodel.model;

import java.util.Collections;
import java.util.Map;
import java.util.Objects;

public class NestedError {
    private String fieldName;
    private String message;
    private String code;
    private Map<String, String> params;

    public NestedError() {
        params = Collections.emptyMap();
    }

    public NestedError(String fieldName, String message, String code, Map<String, String> params) {
        this.fieldName = fieldName;
        this.message = message;
        this.code = code;
        this.params = Collections.unmodifiableMap(params);
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Map<String, String> getParams() {
        return Collections.unmodifiableMap(params);
    }

    public void setParams(Map<String, String> params) {
        this.params = Collections.unmodifiableMap(params);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NestedError that = (NestedError) o;
        return Objects.equals(fieldName, that.fieldName)
            && Objects.equals(message, that.message)
            && Objects.equals(code, that.code)
            && Objects.equals(params, that.params);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fieldName, message, code, params);
    }

}
