package io.supercharge.common.errorhandlermodel.model.response;

import static java.util.Objects.nonNull;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.supercharge.common.errorhandlermodel.model.DebugError;
import io.supercharge.common.errorhandlermodel.model.NestedError;

public class ErrorResponse {
    private String requestId;
    private String message;
    private String code;
    private List<NestedError> nestedErrors;
    private Map<String, String> params;
    private DebugError debugError;

    public ErrorResponse() {
    }

    public ErrorResponse(
        String requestId,
        String message,
        String code,
        List<NestedError> nestedErrors,
        Map<String, String> params,
        DebugError debugError
    ) {
        this.requestId = requestId;
        this.message = message;
        this.code = code;
        this.nestedErrors = nestedErrors;
        this.params = Collections.unmodifiableMap(params);
        this.debugError = debugError;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<NestedError> getNestedErrors() {
        return nestedErrors;
    }

    public void setNestedErrors(List<NestedError> nestedErrors) {
        this.nestedErrors = nestedErrors;
    }

    public Map<String, String> getParams() {
        return Collections.unmodifiableMap(params);
    }

    public void setParams(Map<String, String> params) {
        this.params = Collections.emptyMap();
        if (nonNull(params)) {
            this.params = Collections.unmodifiableMap(params);
        }
    }

    public DebugError getDebugError() {
        return debugError;
    }

    public void setDebugError(DebugError debugError) {
        this.debugError = debugError;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ErrorResponse that = (ErrorResponse) o;
        return Objects.equals(requestId, that.requestId)
            && Objects.equals(message, that.message)
            && Objects.equals(code, that.code)
            && Objects.equals(nestedErrors, that.nestedErrors)
            && Objects.equals(params, that.params);
    }

    @Override
    public int hashCode() {
        return Objects.hash(requestId, message, code, nestedErrors, params);
    }

}
