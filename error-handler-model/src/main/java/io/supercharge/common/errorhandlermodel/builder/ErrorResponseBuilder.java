package io.supercharge.common.errorhandlermodel.builder;

import static java.util.Objects.nonNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.supercharge.common.errorhandlermodel.model.DebugError;
import io.supercharge.common.errorhandlermodel.model.NestedError;
import io.supercharge.common.errorhandlermodel.model.response.ErrorResponse;

public final class ErrorResponseBuilder {
    private String requestId;
    private String message;
    private String code;
    private List<NestedError> nestedErrors;
    private final Map<String, String> params;
    private DebugError debugError;

    private ErrorResponseBuilder() {
        params = new HashMap<>();
    }

    public static ErrorResponseBuilder anErrorResponse() {
        return new ErrorResponseBuilder();
    }

    //CHECKSTYLE:OFF
    public ErrorResponseBuilder withRequestId(String requestId) {
        this.requestId = requestId;
        return this;
    }

    public ErrorResponseBuilder withMessage(String message) {
        this.message = message;
        return this;
    }

    public ErrorResponseBuilder withCode(String code) {
        this.code = code;
        return this;
    }

    public ErrorResponseBuilder withNestedErrors(List<NestedError> nestedErrors) {
        this.nestedErrors = nestedErrors;
        return this;
    }

    public ErrorResponseBuilder withDebugError(DebugError debugError) {
        this.debugError = debugError;
        return this;
    }

    public ErrorResponseBuilder withParam(String name, String value) {
        params.put(name, value);
        return this;
    }

    public ErrorResponseBuilder withParams(Map<String, String> additionalParams) {
        if (nonNull(additionalParams)) {
            params.putAll(additionalParams);
        }
        return this;
    }
    //CHECKSTYLE:ON

    public ErrorResponse build() {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setRequestId(requestId);
        errorResponse.setMessage(message);
        errorResponse.setCode(code);
        errorResponse.setNestedErrors(nestedErrors);
        errorResponse.setParams(params);
        errorResponse.setDebugError(debugError);
        return errorResponse;
    }
}
