package io.supercharge.common.errorhandlermodel.builder;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import io.supercharge.common.errorhandlermodel.model.DebugError;
import io.supercharge.common.errorhandlermodel.model.response.ErrorResponse;

public class ErrorResponseBuilderTest {

    private static final String MESSAGE = "Error Response Message";
    private static final String CODE = "ERR_RESP_CODE";
    private static final String REQUEST_ID = "REQ_12345";
    private static final DebugError DEBUG_ERROR = new DebugError();
    private static final String PARAM1 = "Param1";
    private static final String VALUE1 = "Value1";
    private static final String PARAM2 = "Param2";
    private static final String VALUE2 = "Value2";
    private static final String PARAM3 = "Param3";
    private static final String VALUE3 = "Value3";
    private static final Map<String, String> PARAMS = new HashMap<>();

    static {
        PARAMS.put(PARAM1, VALUE1);
        PARAMS.put(PARAM2, VALUE2);
    }

    private static final Map<String, String> EXPECTED_PARAMS = new HashMap<>();

    static {
        EXPECTED_PARAMS.put(PARAM1, VALUE1);
        EXPECTED_PARAMS.put(PARAM2, VALUE2);
        EXPECTED_PARAMS.put(PARAM3, VALUE3);
    }

    @Test
    @DisplayName("Should build ErrorResponse with the provided parameters")
    void errorResponseBuilderTest() {
        //WHEN
        final ErrorResponse actual = ErrorResponseBuilder.anErrorResponse()
            .withMessage(MESSAGE)
            .withCode(CODE)
            .withRequestId(REQUEST_ID)
            .withDebugError(DEBUG_ERROR)
            .withParams(PARAMS)
            .withParam(PARAM3, VALUE3)
            .build();
        //THEN
        assertThat(actual).isNotNull();
        assertThat(actual.getMessage()).isEqualTo(MESSAGE);
        assertThat(actual.getCode()).isEqualTo(CODE);
        assertThat(actual.getRequestId()).isEqualTo(REQUEST_ID);
        assertThat(actual.getDebugError()).isSameAs(DEBUG_ERROR);
        assertThat(actual.getParams()).containsExactlyInAnyOrderEntriesOf(EXPECTED_PARAMS);
    }

    @Test
    @DisplayName("Should build ErrorResponse without params")
    void errorResponseBuilderWithoutParamsTest() {
        //WHEN
        final ErrorResponse actual = ErrorResponseBuilder.anErrorResponse()
            .build();
        //THEN
        assertThat(actual).isNotNull();
        assertThat(actual.getMessage()).isNull();
        assertThat(actual.getCode()).isNull();
        assertThat(actual.getRequestId()).isNull();
        assertThat(actual.getDebugError()).isNull();
        assertThat(actual.getParams()).isEmpty();
    }
}
