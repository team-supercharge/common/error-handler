package io.supercharge.common.errorhandlermodel.builder;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import io.supercharge.common.errorhandlermodel.model.NestedError;

public class NestedErrorBuilderTest {

    private static final String FIELD_NAME = "testFieldName";
    private static final String MESSAGE = "Test Error Message";
    private static final String PARAM1 = "NestedParam1";
    private static final String VALUE1 = "NestedValue1";
    private static final String PARAM2 = "NestedParam2";
    private static final String VALUE2 = "NestedValue2";
    private static final String PARAM3 = "NestedParam3";
    private static final String VALUE3 = "NestedValue3";
    private static final Map<String, String> PARAMS = new HashMap<>();

    static {
        PARAMS.put(PARAM1, VALUE1);
        PARAMS.put(PARAM2, VALUE2);
    }

    private static final Map<String, String> EXPECTED_PARAMS = new HashMap<>();

    static {
        EXPECTED_PARAMS.put(PARAM1, VALUE1);
        EXPECTED_PARAMS.put(PARAM2, VALUE2);
        EXPECTED_PARAMS.put(PARAM3, VALUE3);
    }

    private static final String CODE = "ERR_CODE";

    @Test
    @DisplayName("Should build NestedError with provided params.")
    void nestedErrorBuilderTest() {
        //WHEN
        final NestedError actual = NestedErrorBuilder.aNestedError()
            .withFieldName(FIELD_NAME)
            .withMessage(MESSAGE)
            .withParams(PARAMS)
            .withParam(PARAM3, VALUE3)
            .withCode(CODE)
            .build();
        //THEN
        assertThat(actual).isNotNull();
        assertThat(actual.getFieldName()).isEqualTo(FIELD_NAME);
        assertThat(actual.getMessage()).isEqualTo(MESSAGE);
        assertThat(actual.getParams()).containsExactlyInAnyOrderEntriesOf(EXPECTED_PARAMS);
        assertThat(actual.getCode()).isEqualTo(CODE);
    }

    @Test
    @DisplayName("Should build NestedError / without params.")
    void nestedErrorBuilderWithoutParamsTest() {
        //WHEN
        final NestedError actual = NestedErrorBuilder.aNestedError()
            .build();
        //THEN
        assertThat(actual).isNotNull();
        assertThat(actual.getFieldName()).isNull();
        assertThat(actual.getMessage()).isNull();
        assertThat(actual.getParams()).isEmpty();
        assertThat(actual.getCode()).isNull();
    }
}
