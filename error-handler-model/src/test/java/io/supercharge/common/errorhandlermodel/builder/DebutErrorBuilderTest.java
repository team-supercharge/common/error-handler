package io.supercharge.common.errorhandlermodel.builder;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import io.supercharge.common.errorhandlermodel.model.DebugError;

public class DebutErrorBuilderTest {

    private static final String REQUEST_BODY = "Test error request body.";
    private static final String REQUEST_HEADERS = "Test request headers";
    private static final String STACK_TRACE = "STACK TRACE";
    private static final long TIMESTAMP = 123L;
    private static final int STATUS_CODE = 401;
    private static final String URL = "http://localhost";

    @Test
    @DisplayName("Should build DebugError with the provided parameters.")
    void debugErrorBuilderTest() {
        //WHEN
        final DebugError actual = DebugErrorBuilder.aDebugError()
            .withRequestBody(REQUEST_BODY)
            .withRequestHeaders(REQUEST_HEADERS)
            .withStacktrace(STACK_TRACE)
            .withTimestamp(TIMESTAMP)
            .withUpstreamStatusCode(STATUS_CODE)
            .withUrl(URL)
            .build();
        //THEN
        assertThat(actual).isNotNull();
        assertThat(actual.getRequestBody()).isEqualTo(REQUEST_BODY);
        assertThat(actual.getRequestHeaders()).isEqualTo(REQUEST_HEADERS);
        assertThat(actual.getStacktrace()).isEqualTo(STACK_TRACE);
        assertThat(actual.getTimestamp()).isEqualTo(TIMESTAMP);
        assertThat(actual.getUpstreamStatusCode()).isEqualTo(STATUS_CODE);
        assertThat(actual.getUrl()).isEqualTo(URL);
    }

    @Test
    @DisplayName("Should build DebugError / without params.")
    void debugErrorBuilderWithoutParamsTest() {
        //WHEN
        final DebugError actual = DebugErrorBuilder.aDebugError()
            .build();
        //THEN
        assertThat(actual).isNotNull();
        assertThat(actual.getRequestBody()).isNull();
        assertThat(actual.getRequestHeaders()).isNull();
        assertThat(actual.getStacktrace()).isNull();
        assertThat(actual.getTimestamp()).isNull();
        assertThat(actual.getUpstreamStatusCode()).isNull();
        assertThat(actual.getUrl()).isNull();
    }
}
