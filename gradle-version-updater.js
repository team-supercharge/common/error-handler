const versionRe = /^version\s*=\s*"(.*)"$/m;

module.exports.readVersion = function (contents) {
    const version = contents.match(versionRe);
    if (version === null) {
        throw "Could not match version in gradle bump file";
    }
    return version[1];
};

module.exports.writeVersion = function (contents, version) {
    return contents.replace(versionRe, "version = \"" + version + "\"");
};
