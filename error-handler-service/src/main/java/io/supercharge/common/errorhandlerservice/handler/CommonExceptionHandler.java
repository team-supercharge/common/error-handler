package io.supercharge.common.errorhandlerservice.handler;

import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import io.supercharge.common.errorhandlermodel.builder.DebugErrorBuilder;
import io.supercharge.common.errorhandlermodel.builder.ErrorResponseBuilder;
import io.supercharge.common.errorhandlermodel.builder.NestedErrorBuilder;
import io.supercharge.common.errorhandlermodel.model.DebugError;
import io.supercharge.common.errorhandlermodel.model.NestedError;
import io.supercharge.common.errorhandlermodel.model.response.ErrorResponse;

public abstract class CommonExceptionHandler extends ResponseEntityExceptionHandler {

    protected ResponseEntity<Object> handleException(
        Exception exception,
        ErrorResponse body,
        HttpStatus status,
        WebRequest request
    ) {
        HttpHeaders headers = new HttpHeaders();
        return handleExceptionInternal(
            exception,
            body,
            headers,
            Objects.requireNonNull(status),
            request
        );
    }

    public ErrorResponse errorResponseMapper(String errorCode, String message, DebugError debugError) {
        return errorResponseMapper(errorCode, message, null, debugError);
    }

    public ErrorResponse errorResponseMapper(
        String errorCode,
        String message,
        List<NestedError> nestedErrors,
        DebugError debugError
    ) {
        ErrorResponse errorResponse = ErrorResponseBuilder.anErrorResponse()
            .withCode(errorCode)
            .withMessage(message)
            .withDebugError(debugError)
            .build();
        if (Objects.nonNull(nestedErrors) && !nestedErrors.isEmpty()) {
            errorResponse.setNestedErrors(nestedErrors);
        }
        return errorResponse;
    }

    public DebugError debugErrorMapper(
        Exception exception,
        String header,
        String body,
        HttpStatus httpStatus
    ) {
        return DebugErrorBuilder
            .aDebugError()
            .withRequestBody(body)
            .withRequestHeaders(header)
            .withUpstreamStatusCode(httpStatus.value())
            .withStacktrace(Stream.of(exception.getStackTrace())
                .map(StackTraceElement::toString)
                .collect(Collectors.joining("\n")))
            .withTimestamp(Instant.now().toEpochMilli())
            .build();
    }

    public NestedError nestedErrorMapper(String fieldName, String errorCode, String message) {
        return NestedErrorBuilder.aNestedError()
            .withFieldName(fieldName)
            .withCode(errorCode)
            .withMessage(message)
            .build();
    }

}
