package io.supercharge.common.errorhandlerservice.handler;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

import java.util.Arrays;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import org.springframework.http.HttpStatus;

import io.supercharge.common.errorhandlermodel.model.DebugError;
import io.supercharge.common.errorhandlermodel.model.NestedError;
import io.supercharge.common.errorhandlermodel.model.response.ErrorResponse;

public class CommonExceptionHandlerTest {

    //debug error
    private static final String ERROR_HEADER = "Test Error Header";
    private static final String ERROR_BODY = "Test error body";
    private static final HttpStatus ERROR_STATUS = HttpStatus.UNAUTHORIZED;
    private static final String STACK_TRACE_ELEMENT_1 = "Stack trace element 1";
    private static final String STACK_TRACE_ELEMENT_2 = "Stack trace element 2";
    private static final String STACK_TRACE_STR = STACK_TRACE_ELEMENT_1 + "\n" + STACK_TRACE_ELEMENT_2;

    //nested error/error response
    private static final String FIELD_NAME = "test_field";
    private static final String ERROR_CODE = "ERR_12345";
    private static final String ERROR_MESSAGE = "Test error message.";

    private CommonExceptionHandler commonExceptionHandler = new CommonExceptionHandler() { };

    @Test
    @DisplayName("Should create the proper DebugError from the provided input.")
    void debugErrorMapperTest() {
        //GIVEN
        final Exception exception = mock(Exception.class);
        final StackTraceElement stackTraceElement1 = mock(StackTraceElement.class);
        final StackTraceElement stackTraceElement2 = mock(StackTraceElement.class);
        given(stackTraceElement1.toString()).willReturn(STACK_TRACE_ELEMENT_1);
        given(stackTraceElement2.toString()).willReturn(STACK_TRACE_ELEMENT_2);
        given(exception.getStackTrace()).willReturn(new StackTraceElement[] {stackTraceElement1, stackTraceElement2 });
        //WHEN
        final DebugError actual = commonExceptionHandler.debugErrorMapper(
            exception,
            ERROR_HEADER,
            ERROR_BODY,
            ERROR_STATUS
        );
        //THEN
        assertThat(actual).isNotNull();
        assertThat(actual.getRequestBody()).isEqualTo(ERROR_BODY);
        assertThat(actual.getRequestHeaders()).isEqualTo(ERROR_HEADER);
        assertThat(actual.getTimestamp()).isNotZero();
        assertThat(actual.getUpstreamStatusCode()).isEqualTo(ERROR_STATUS.value());
        assertThat(actual.getStacktrace()).isEqualTo(STACK_TRACE_STR);
    }

    @Test
    @DisplayName("Should create the proper NestedError from the provided input.")
    void nestedErrorMapperTest() {
        //WHEN
        final NestedError actual = commonExceptionHandler.nestedErrorMapper(FIELD_NAME, ERROR_CODE, ERROR_MESSAGE);
        //THEN
        assertThat(actual).isNotNull();
        assertThat(actual.getCode()).isEqualTo(ERROR_CODE);
        assertThat(actual.getFieldName()).isEqualTo(FIELD_NAME);
        assertThat(actual.getMessage()).isEqualTo(ERROR_MESSAGE);
        assertThat(actual.getParams()).isEmpty();
    }

    @Test
    @DisplayName("Should create the proper ErrorResponse from the provided input / null nested errors.")
    void errorResponseMapperTest() {
        //GIVEN
        final DebugError debugError = new DebugError();
        //WHEN
        final ErrorResponse actual = commonExceptionHandler.errorResponseMapper(
            ERROR_CODE,
            ERROR_MESSAGE,
            null,
            debugError
        );
        //THEN
        assertThat(actual).isNotNull();
        assertThat(actual.getDebugError()).isSameAs(debugError);
        assertThat(actual.getNestedErrors()).isNullOrEmpty();
        assertThat(actual.getMessage()).isEqualTo(ERROR_MESSAGE);
        assertThat(actual.getCode()).isEqualTo(ERROR_CODE);
    }

    @Test
    @DisplayName("Should create the proper ErrorResponse from the provided input.")
    void errorResponseMapperWithNestedErrorsTest() {
        //GIVEN
        final DebugError debugError = new DebugError();
        final NestedError nestedError1 = new NestedError();
        final NestedError nestedError2 = new NestedError();
        //WHEN
        final ErrorResponse actual = commonExceptionHandler.errorResponseMapper(
            ERROR_CODE,
            ERROR_MESSAGE,
            Arrays.asList(nestedError1, nestedError2),
            debugError
        );
        //THEN
        assertThat(actual).isNotNull();
        assertThat(actual.getDebugError()).isSameAs(debugError);
        assertThat(actual.getNestedErrors()).containsExactly(nestedError1, nestedError2);
        assertThat(actual.getMessage()).isEqualTo(ERROR_MESSAGE);
        assertThat(actual.getCode()).isEqualTo(ERROR_CODE);
    }

    @Test
    @DisplayName("Should create the proper ErrorResponse from the provided input / without nested errors.")
    void errorResponseMapperWithoutNestedErrorsTest() {
        //GIVEN
        final DebugError debugError = new DebugError();
        //WHEN
        final ErrorResponse actual = commonExceptionHandler.errorResponseMapper(
            ERROR_CODE,
            ERROR_MESSAGE,
            debugError
        );
        //THEN
        assertThat(actual).isNotNull();
        assertThat(actual.getDebugError()).isSameAs(debugError);
        assertThat(actual.getNestedErrors()).isNullOrEmpty();
        assertThat(actual.getMessage()).isEqualTo(ERROR_MESSAGE);
        assertThat(actual.getCode()).isEqualTo(ERROR_CODE);
    }
}
