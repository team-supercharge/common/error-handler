# Supercharge Common Error Handling Library

This repository contains the necessary models (POJOs) and a
Spring Exception handler base class for common error responses that 
should be returned by REST APIs.

## Getting started

### Add dependency

* Gradle (`build.gradle.kts`):

```kotlin
val scErrorHandlerVersion: String by project
implementation("io.supercharge.common:error-handler-model:$scErrorHandlerVersion")
implementation("io.supercharge.common:error-handler-service:$scErrorHandlerVersion")
```

### Create a custom exception handler

Extend the `CommonExceptionHandler`, catch your domain exceptions and translate them to error responses:

```java
import io.supercharge.common.errorhandlerservice.handler.CommonExceptionHandler;

@RestControllerAdvice
class ExceptionHandlerAdvice extends CommonExceptionHandler {
    
    @ExceptionHandler(DomainException.class)
    public ResponseEntity<Object> handleDomainException(DomainException exception, WebRequest request) {
        ErrorResponse errorResponse = ErrorResponseBuilder.anErrorResponse()
            .withCode(errorCode)
            .withMessage(message)
            .withDebugError(debugError)
            .build();
        return handleExceptionInternal(exception, errorResponse, HttpHeaders.EMPTY, HttpStatus.BAD_REQUEST, request);
    }
}
```

### Build error responses

Convenient builder classes are provided for assembling error messages: 

```java
ErrorResponseBuilder.anErrorResponse()
        .withCode("CLIENT_NOT_FOUND")
        .withRequestId(requestId)
        .withMessage("The client was not found")
        .build();
```

If you would like to include additional error details in the response you can do it using the `NestedErrorBuilder`
class:

```java
DebugError debugError = null;

if (debug) {
    debugError = DebugErrorBuilder.aDebugError()
            .withRequestBody(requestBody)
            .withStacktrace(stacktrace)
            .withUpstreamStatusCode(502)
            .withTimestamp(Instant.now().toEpochMilli())
            .build();        
}

ErrorResponseBuilder.anErrorResponse()
        .withCode("CLIENT_NOT_FOUND")
        .withRequestId(requestId)
        .withMessage("The client was not found")
        .withDebugError(debugError)
        .build();
```

## Migration

### From 0.0.x to 1.0.0

* Replace dependency
* Replace packages `io.supercharge.launchpad2.errorhandlerservice` and `io.supercharge.launchpad2.errorhandlermodel`
  with `io.supercharge.common.errorhandlerservice` and `io.supercharge.common.errorhandlermodel`

## Development

The library can be built with `./gradlew build`. To install it to your local
maven repository use `./gradlew install`
