# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.0.0](https://gitlab.com/team-supercharge/common/error-handler/compare/v0.0.5...v1.0.0) (2022-05-06)


### ⚠ BREAKING CHANGES

* moves classes to common module

* moves classes to common module ([e4a4fd1](https://gitlab.com/team-supercharge/common/error-handler/commit/e4a4fd1df983eaa53d49e6841d1b0ba167baaa4f))

### 0.0.5 (2022-05-05)


### Features

* added params for error and nested error response 3d9c598
